package main.java.flows.csdb;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

import encryptusercredentials.EncryptCredentails;
import main.java.base.TestBase;
import main.java.pages.csdb.CSDBCustomerDetailsPage;
import main.java.pages.csdb.CSDBLandingPage;
import main.java.pages.csdb.CustomerOrderDetails;
import main.java.utils.CommonFunctions;


public class CSDBPatientDetailsFlow extends TestBase{
	
	private JavascriptExecutor jse = null;

	public CSDBPatientDetailsFlow() {
		jse = (JavascriptExecutor) driver;
		PageFactory.initElements(new AjaxElementLocatorFactory(driver, 10), this);
	}

	JavascriptExecutor e = (JavascriptExecutor) driver;
	
	/**
	 * This Method for CSDB Login page
	 * 
	 * @throws Exception
	 *
	 */
	
	public static void loginCSDBPage() throws Exception {
		new CSDBLandingPage().verifyCSDBPage();
		new CSDBLandingPage().enterCSDBLogin(new EncryptCredentails()
				.decrypt(CommonFunctions.getPropertyValues().getProperty("csdbusername")));
		new CSDBLandingPage().enterCSDBPassword(new EncryptCredentails()
				.decrypt(CommonFunctions.getPropertyValues().getProperty("csdbpassword"))); 
		new CSDBLandingPage().clickCSDBLogin();
		
	}
	
	/**
	 * This Method for verifying Customer Details
	 * 
	 * @param usermail
	 * 
	 * @throws Exception
	 *
	 */
	
	public static void verifyCustomerDetails(String usermail) throws Exception {
		new CSDBLandingPage().verifyLandingPage();
		new CSDBLandingPage().clickCustomerDataBase();
		new CSDBLandingPage().verifyCustomerDataList();
		new CSDBLandingPage().enterCSDBUserSearch(usermail);
		new CSDBLandingPage().clickCSDBFirstCustomer();
		new CSDBCustomerDetailsPage().verifyCSDBCustommerPersonalDetails(usermail);
		new CSDBCustomerDetailsPage().verifyCustomerContactDetails();
		new CustomerOrderDetails().enterCSDBUserSearch(OrderId);
		new CustomerOrderDetails().clickCSDBFirstOrder();
		new CustomerOrderDetails().verifyCSDBOrderDetailPage();
		new CustomerOrderDetails().verifyCustomerOrderDetails();
	}

}
