package main.java.utils;

import java.io.IOException;

import org.testng.annotations.Test;

import main.java.base.TestBase;
import main.java.utils.reportutil.ExtentManager;

public class TestCreator extends TestBase{
	
	@Test
	public void initializer() throws Exception {
		//CommonFunctions.killBrowser();
	}
	
	@Test
	public void reportCreator() throws IOException {
		new TestBase().deleteReportFiles();
		extent = ExtentManager.createInstance();
		extent.flush();
	}
}
