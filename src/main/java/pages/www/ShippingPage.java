package main.java.pages.www;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

import main.java.base.TestBase;
import main.java.utils.CommonFunctions;

public class ShippingPage extends TestBase {

	private JavascriptExecutor jse = null;

	@FindBy(xpath = "//button[text()='Continue to shipping']")
	private WebElement continueShopping;

	@FindBy(xpath = "//button[text()='Continue to payment']")
	private WebElement continuePayment;

	@FindBy(xpath = "//h3[text()='FedEx First Overnight']//parent::div//ancestor::label")
	private WebElement fedxFirst;

	@FindBy(xpath = "//h3[text()='FedEx Priority Overnight']//parent::div//ancestor::label")
	private WebElement fedxPriority;

	@FindBy(xpath = "//h3[text()='FedEx 2Day']//parent::div//ancestor::label")
	private WebElement Fedx2Day;

	@FindBy(xpath = "(//*[contains(text(),'Choose your shipping method')]//parent::div//label)[1]")
	private WebElement firstShippingMethod;

	public ShippingPage() {
		jse = (JavascriptExecutor) driver;
		PageFactory.initElements(new AjaxElementLocatorFactory(driver, 10), this);
	}

	JavascriptExecutor e = (JavascriptExecutor) driver;

	/**
	 * Method to click the continue to shipping in shipping page
	 * 
	 * 
	 * @throws Exception
	 */

	public void continueToShipping() throws Exception {

		CommonFunctions.elementToBeClickable(continueShopping, "continue shopping button");
		WebElement continueToShopping = driver.findElement(By.xpath("//button[text()='Continue to shipping']"));
		continueToShopping.click();
		CommonFunctions.logMessage("continue shopping button is clicked");
	}

	/**
	 * Method to select the shipping method in shipping page
	 * 
	 * @param shipType
	 * 
	 * 
	 * @throws Exception
	 */

	public void selectShippingMethod(String shipType) throws Exception {

		switch (shipType) {

		case "0":
			CommonFunctions.clickWebelement(firstShippingMethod, "first shipping");
			orderShippingRate = CommonFunctions.getTextOfElement(firstShippingMethod, "first shipping");
			break;

		case "1":
			CommonFunctions.clickWebelement(fedxFirst, "fedx first");
			orderShippingRate = CommonFunctions.getTextOfElement(fedxFirst, "fedx first");
			break;
			
		case "2":
			CommonFunctions.clickWebelement(fedxPriority, "fedx priority");
			orderShippingRate = CommonFunctions.getTextOfElement(fedxPriority, "fedx priority");
			break;
			
		case "3":
			CommonFunctions.clickWebelement(Fedx2Day, "Fedx 2Day");
			orderShippingRate = CommonFunctions.getTextOfElement(Fedx2Day, "Fedx 2Day");
			break;
			
		default:
			CommonFunctions.logErrorMessagestopExecution("The shipping method selected is not exist " + shipType);
			break;
		}

		orderShippingRate = CommonFunctions.regexText("\\$(\\d+?[\\.\\d]+)", orderShippingRate).replace("$", "");
		CommonFunctions.logMessage("Cost of shipping method selected " + orderShippingRate);

	}

	/**
	 * Method to click continue to payment in shipping page
	 * 
	 * 
	 * @throws Exception
	 */

	public void continueToPayment() throws Exception {
		CommonFunctions.elementToBeClickable(continuePayment, "continue payment button");
		WebElement continueToPayment = driver.findElement(By.xpath("//button[text()='Continue to payment']"));
		continueToPayment.click();
		CommonFunctions.logMessage("continue payment button is clicked");
	}

}
