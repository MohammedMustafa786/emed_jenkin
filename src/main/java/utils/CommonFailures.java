package main.java.utils;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import main.java.base.TestBase;

public class CommonFailures extends TestBase {

	private static JavascriptExecutor jse = null;

	@SuppressWarnings("static-access")
	public CommonFailures(WebDriver driver) {
		this.driver = driver;
		jse = (JavascriptExecutor) driver;
	}

	/**
	 * This Method includes all common failure
	 *
	 */

	public boolean commonErrorsCaptures() throws Exception {

		driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
		boolean flag = false;

		List<WebElement> verificationCodeError = driver
				.findElements(By.xpath("//*[contains(text(),'Please resend a code to receive')]"));

		List<WebElement> requiredFieldsError = driver.findElements(
				By.xpath("//*[contains(text(),'Please fill in all of the required fields to continue')]"));

		List<WebElement> loginCredMisMatch = driver
				.findElements(By.xpath("//*[contains(text(),'The email and password you’ve entered doesn’t match')]"));

		List<WebElement> meetingNotEstablished = driver
				.findElements(By.xpath("//*[contains(text(),'Meeting could not be established')]"));

		List<WebElement> passwordResetError = driver
				.findElements(By.xpath("//*[contains(text(),'There was an error submitting your email')]"));

		List<WebElement> proctorLoginError = driver
				.findElements(By.xpath("//*[contains(text(),'Could not sign you in')]"));

		List<WebElement> profileDataSubmitError = driver.findElements(
				By.xpath("//*[contains(text(),'An error was encountered when submitting your profile data')]"));

		if (verificationCodeError.size() > 0) {
			flag = true;
			CommonFunctions.logErrorMessage("Error Please resend a code to receive a new verification code..");
			throw new Exception();

		} else if (requiredFieldsError.size() > 0) {
			flag = true;
			CommonFunctions.logErrorMessage(
					"Error Please fill in all of the required fields to continue the sign up process..");
			throw new Exception();

		} else if (loginCredMisMatch.size() > 0) {
			flag = true;
			CommonFunctions.logErrorMessage("Error The email and password you’ve entered doesn’t match any account..");
			throw new Exception();

		} else if (meetingNotEstablished.size() > 0) {
			flag = true;
			CommonFunctions.logErrorMessage("Error Oops! Meeting could not be established..");
			throw new Exception();

		} else if (passwordResetError.size() > 0) {
			flag = true;
			CommonFunctions
					.logErrorMessage("Error There was an error submitting your email address. Please try again.");
			throw new Exception();

		} else if (proctorLoginError.size() > 0) {
			flag = true;
			CommonFunctions.logErrorMessage("Error Could not sign you in.");
			throw new Exception();
			
		} else if (profileDataSubmitError.size() > 0) {
			flag = true;
			CommonFunctions.logErrorMessage("An error was encountered when submitting your profile data. Please try again.");
			throw new Exception();
		}
		return flag;
	}

}
