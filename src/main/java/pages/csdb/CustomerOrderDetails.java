package main.java.pages.csdb;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

import main.java.base.TestBase;
import main.java.utils.CommonFunctions;

public class CustomerOrderDetails extends TestBase {

	private JavascriptExecutor jse = null;

	@FindBy(xpath = "//input[@placeholder='Search by order ID']")
	private WebElement searchOrderId;

	@FindBy(xpath = "(//*[text()='Order ID']//following::tbody//td)[3]")
	private WebElement customerFirstOrder;

	@FindBy(xpath = "//a//*[text()='Order Details']")
	private WebElement orderDetailsBreadCrumb;
	
	@FindBy(xpath = "//*[text()='Order Id']//following-sibling::p")
	private WebElement customerOrderNumber;
	
	@FindBy(xpath = "//*[text()='Order Date']//following-sibling::p")
	private WebElement customerOrderDate;
	
	@FindBy(xpath = "//*[text()='Subtotal']//parent::div//following-sibling::div")
	private WebElement csdbSubTotal;
	
	@FindBy(xpath = "//*[text()='Shipping']//parent::div//following-sibling::div")
	private WebElement csdbShippingPrice;
	
	@FindBy(xpath = "//*[text()='Tax']//parent::div//following-sibling::div")
	private WebElement csdbTax;
	
	@FindBy(xpath = "//*[text()='Total']//parent::div//following-sibling::div")
	private WebElement csdbTotalPrice;

	public CustomerOrderDetails() {
		jse = (JavascriptExecutor) driver;
		PageFactory.initElements(new AjaxElementLocatorFactory(driver, 10), this);
	}

	JavascriptExecutor e = (JavascriptExecutor) driver;

	/**
	 * This Method to search the CSDB user
	 * 
	 *
	 * @throws Exception
	 */

	public void enterCSDBUserSearch(String orderId) throws Exception {
		CommonFunctions.Sendkeys(searchOrderId, orderId, "CSDB patient order Id");
	}

	/**
	 * This Method click the CSDB First customer order
	 * 
	 * @throws Exception
	 */

	public void clickCSDBFirstOrder() throws Exception {
		CommonFunctions.actionClick(customerFirstOrder, "CSDB first order");
	}

	/**
	 * This Method for verifying the CSDB Order Details Page
	 * 
	 * @throws Exception
	 *
	 */

	public void verifyCSDBOrderDetailPage() throws Exception {
		CommonFunctions.waitForPageLoad(driver);
		CommonFunctions.elementIsVisible(orderDetailsBreadCrumb, "order details page");
		CommonFunctions.logMessage("<-----CSDB Order Details Page----->");
	}

	/**
	 * @apiNote Method is used to verify the Customer order details in CSDB
	 * @throws Exception
	 */
	public void verifyCustomerOrderDetails() throws Exception {

		CommonFunctions.logMessage("**************Order Details Verification************");
		String orderDateSplit[] = CommonFunctions.getTextOfElement(customerOrderDate, "Customer order date").split("/");
		if (Integer.parseInt(orderDateSplit[1]) <= 9) {
			orderDateSplit[1] = "0" + orderDateSplit[1];
		}
		String orderDate = orderDateSplit[0] + "/" + orderDateSplit[1] + "/" + orderDateSplit[2];
		String orderNumber = CommonFunctions.getTextOfElement(customerOrderNumber, "Customer order number");
		String subTotal = CommonFunctions.getTextOfElement(csdbSubTotal, "sub total").replace("$", "");
		String shippingCost = CommonFunctions.getTextOfElement(csdbShippingPrice, "shipping cost").replace("$", "");
		String tax = CommonFunctions.getTextOfElement(csdbTax, "tax rate").replace("$", "");
		String totalCost = CommonFunctions.getTextOfElement(csdbTotalPrice, "total order price").replace("$", "");
		
		if (orderNumber.equalsIgnoreCase(OrderId)) {
			CommonFunctions.logMessage("Order placed number matched the CSDB order number ===> " + orderNumber);
		} else {
			CommonFunctions.logErrorMessage(
					"Order placed number " + confirmationNum + " not matched CSDB order number " + orderNumber);
		}

		if (orderDate.equalsIgnoreCase(orderedDate)) {
			CommonFunctions.logMessage("Order placed date matched the CSDB order date ===> " + orderDate);
		} else {
			CommonFunctions.logErrorMessage(
					"Order placed date " + orderedDate + " not matched CSDB order date " + orderDate);
		}

		if (subTotal.equalsIgnoreCase(orderSubRate)) {
			CommonFunctions.logMessage("Order placed subtotal matched the CSDB order subtotal ===> " + subTotal);
		} else {
			CommonFunctions.logErrorMessage(
					"Order placed subtotal " + orderSubRate + " not matched CSDB order subtotal " + subTotal);
		}

		if (shippingCost.equalsIgnoreCase(orderShippingRate)) {
			CommonFunctions.logMessage(
					"Order placed shipping price matched the CSDB order shipping price ===> " + shippingCost);
		} else {
			CommonFunctions.logErrorMessage("Order placed shipping price " + orderShippingRate
					+ " not matched CSDB order shipping price " + shippingCost);
		}

		if (tax.equalsIgnoreCase(orderTaxRate)) {
			CommonFunctions.logMessage("Order placed shipping tax matched the CSDB order shipping tax ===> " + tax);
		} else {
			CommonFunctions.logErrorMessage(
					"Order placed shipping tax " + orderTaxRate + " not matched CSDB order shipping tax " + tax);
		}

		if (totalCost.equalsIgnoreCase(orderTotalAmount)) {
			CommonFunctions.logMessage("Order placed total price matched the CSDB order total price ===> " + totalCost);
		} else {
			CommonFunctions.logErrorMessage("Order placed total price " + orderTotalAmount
					+ " not matched CSDB order total price " + totalCost);
		}
	}
}
