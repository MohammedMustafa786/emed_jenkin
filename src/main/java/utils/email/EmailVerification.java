package main.java.utils.email;

import java.util.List;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import main.java.base.TestBase;
import main.java.utils.CommonFunctions;

public class EmailVerification extends TestBase {

	private JavascriptExecutor jse = null;

	// Yopmail
	@FindBy(xpath = "//h1")
	private static WebElement yopHeader;

	@FindBy(xpath = "//input[@name='login']")
	private static WebElement EmailEntryBox;

	@FindBy(xpath = "//td//following::a[@id='lrefr']")
	private WebElement checkNewMail;

	@FindBy(xpath = "//input[@value='Check Inbox']")
	private static WebElement CheckInboxButton;

	@FindBy(xpath = "//div[@class='um']//a")
	private List<WebElement> EmailList;

	@FindBy(xpath = "//div[@id='mailhaut']")
	private WebElement MailTextHeader;

	@FindBy(xpath = "//td[@valign='middle']//p")
	private WebElement verificationCode;

	// Mailinator
	@FindBy(xpath = "//input[@aria-label='Enter Inbox Name']")
	private WebElement mailinatorInputBox;

	@FindBy(id = "go-to-public")
	private WebElement goButton;

	@FindBy(xpath = "//tr[@ng-repeat]//a")
	private WebElement emailList;

	@FindBy(xpath = "//td[@align='center']//p")
	private WebElement eMedMailinatorVerifyCode;

	public EmailVerification() {
		jse = (JavascriptExecutor) driver;
		PageFactory.initElements(new AjaxElementLocatorFactory(driver, 10), this);
	}

	JavascriptExecutor e = (JavascriptExecutor) driver;

	/**
	 * This method for YOPMailEmail service
	 * 
	 * 
	 * @param emailId
	 *
	 * @throws Exception
	 *
	 */

	public static void YOPMailEmailSearch(String emailId) throws Exception {
		CommonFunctions.logMessage("<------ YOP Mail Verification ------>");
		CommonFunctions.checkCurrentPage(yopHeader, "YOPmail");
		CommonFunctions.sendKeysIndividual(EmailEntryBox, emailId, "Email Address");
		Thread.sleep(3000);
		CommonFunctions.clickWebelement(CheckInboxButton, "Check Box");
		CommonFunctions.checkCurrentPageTitle("Inbox");
	}

	/**
	 * This method for mailDocumentSearch
	 * 
	 * 
	 * @param emailTypeVerify
	 *
	 * @throws Exception
	 */

	public void mailDocumentSearch(String emailTypeVerify) throws Exception {
		CommonFunctions.logMessage("<------ " + emailTypeVerify + "  searching starts ------>");
		Long startTime = System.currentTimeMillis();
		do {
			try {
				int stopMailCheck = 0;
				driver.switchTo().frame("ifinbox");
				if (EmailList.get(0).isDisplayed()) {
					for (int mail = 0; mail < EmailList.size(); mail++) {
						if (EmailList.get(mail).getText().contains(emailTypeVerify)) {
							CommonFunctions.clickWebelement(EmailList.get(mail), emailTypeVerify + " mail");
							CommonFunctions.logMessage("The mail is displaying...");
							stopMailCheck++;
							break;
						}
					}
					if (stopMailCheck == 1) {
						break;
					}

				}
			} catch (Exception e) {
				driver.switchTo().defaultContent();
				checkNewMail.click();
				CommonFunctions.logMessage("The mail is not displayed. So searching.... ");
				CommonFunctions.logMessage("Check new mail button is clicked");
			}
		} while ((System.currentTimeMillis() - startTime) < 100000);

		driver.switchTo().parentFrame();
		driver.switchTo().frame("ifmail");
		if (MailTextHeader.getText().contains(emailTypeVerify + " Confirmation")) {
			CommonFunctions.logMessage("Email verified successfully");
		}
	}

	/**
	 * This method to fetch the EmedVerificationCode
	 * 
	 * 
	 * @throws Exception
	 *
	 *
	 */

	public void fetchEmedVerificationCode() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, 80);
		wait.until(ExpectedConditions.visibilityOf(verificationCode));
		verifyCode = CommonFunctions.getTextOfElement(verificationCode, "emed verification code");

	}

	/*
	 * All Mailinator code
	 */
	public void searchUsingMailinator(String email) throws Exception {
		CommonFunctions.checkCurrentPageTitle("Mailinator");
		CommonFunctions.logMessage("<-----Mailinator Page----->");
		CommonFunctions.sendKeysIndividual(mailinatorInputBox, email, "Email Address");
		Thread.sleep(2000);
		CommonFunctions.clickWebelement(goButton, "Go button");
		CommonFunctions.clickWebelement(emailList, "email inbox");
		driver.switchTo().frame("msg_body");
		verifyCode = CommonFunctions.getTextOfElement(eMedMailinatorVerifyCode, "emed verification code");
		System.out.println("Code  " + verifyCode);
	}

	/**
	 * @author Mohammed Mustafa Fetching the new emed account verification code from
	 *         mail using the javax mail server
	 */
	public String fetchCode() throws Exception {

		String verifyCode = "";
		try {
			verifyCode = CommonFunctions.regexText(">(\\d{6,6})<", MailReader.readMail("Verify your email"))
					.replaceAll(">|<", "");
			CommonFunctions.logMessage("Verification code fetched " + verifyCode);
			return verifyCode;

		} catch (Exception e) {
			CommonFunctions
					.logErrorMessagestopExecution("Unable to fetch the verification code using javax mail server");
		}
		return verifyCode;
	}

	/**
	 * @author Mohammed Mustafa Fetching the reset verification code from mail using
	 *         the javax mail server
	 */
	public String fetchResetCode() throws Exception {

		String reset = "";
		try {
			reset = CommonFunctions.regexText(">(\\d{6,6})<", MailReader.readMail("Reset your email"))
					.replaceAll(">|<", "");
			CommonFunctions.logMessage("Reset code fetched " + reset);
			return reset;

		} catch (Exception e) {
			CommonFunctions.logErrorMessagestopExecution("Unable to fetch the reset code using javax mail server");
		}
		return reset;
	}

}
