package main.java.flows.www;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

import main.java.base.TestBase;
import main.java.pages.www.EMedLoginPage;
import main.java.pages.www.MyOrderPage;

public class CancelationFlow extends TestBase {

	private JavascriptExecutor jse = null;

	public CancelationFlow() {
		jse = (JavascriptExecutor) driver;
		PageFactory.initElements(new AjaxElementLocatorFactory(driver, 10), this);
	}

	JavascriptExecutor e = (JavascriptExecutor) driver;

	/**
	 * KIT cancellation flow method
	 * 
	 * @throws Exception
	 *
	 */

	public static void cancelOpenOrderFlow() throws Exception {
		new EMedLoginPage().clickMyOrder();
		new MyOrderPage().verifyMyOrderPage();
		new MyOrderPage().clickOpenOrder();
		new MyOrderPage().clickOrderNumber(confirmationNum);
		new MyOrderPage().verifyOrderDetails(confirmationNum);
		new MyOrderPage().clickCancelOrder();
		new MyOrderPage().confirmCancelOrder();
		new MyOrderPage().verifyOrderCancel();
		new MyOrderPage().clickGoToMyOrders();
		new MyOrderPage().clickCancelledOrder();
		new MyOrderPage().verifyCanceledOrder(confirmationNum);
	}
}
