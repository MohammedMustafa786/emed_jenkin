package main.java.utils;

import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.github.bonigarcia.wdm.WebDriverManager;
import main.java.base.TestBase;

public class DualDriverTestStart extends TestBase {

	private JavascriptExecutor jse = null;

	@FindBy(xpath = "//p[text()='Login']//parent::a")
	private WebElement loginButton;

	@FindBy(xpath = "//*[contains(text(),'I want to start testing.')]//parent::div")
	private WebElement startTesting;

	@FindBy(xpath = "//button[contains(text(),'Login with Navica')]")
	private WebElement loginViaNavica;

	@FindBy(id = "signInName")
	private WebElement navicaUser;

	@FindBy(id = "password")
	private WebElement navicaPassword;

	@FindBy(xpath = "//button[text()='Sign in']")
	private WebElement navicaSigin;

	@FindBy(xpath = "//a[text()='Start testing']")
	private WebElement startLabTest;

	@FindBy(xpath = "//h1[contains(text(),'This test will take')]")
	private WebElement twentyMinutesTestHeader;

	@FindBy(xpath = "//form//div[@data-testid]//p")
	private List<WebElement> preTestQuestions;

	@FindBy(xpath = "//p[contains(text(),'Waiting on a certified proctor')]")
	private WebElement checkForWait;

	@FindBy(xpath = "//button[text()='Allow']")
	private WebElement navicaPermissionAllow;

	public DualDriverTestStart() {
		jse = (JavascriptExecutor) driver1;
		PageFactory.initElements(new AjaxElementLocatorFactory(driver1, 10), this);
	}
	
	/**
	 * This Method for Driver invocation
	 * 
	 * @param  baseurl
	 *
	 */

	public void driverInvoker(String baseurl) {

		ChromeOptions options = new ChromeOptions();
		HashMap<String, Object> chromePref = new HashMap<String, Object>();
		chromePref.put("profile.default_content_settings.popups", 0);
		options.setExperimentalOption("prefs", chromePref);
		options.addArguments("disable-infobars");
		if (!mode.equalsIgnoreCase("incog")) {
			options.addArguments("--incognito");
		}
		options.addArguments("use-fake-ui-for-media-stream");
		options.addArguments("test-type");
		options.addArguments("ignore-certificate-errors");
		options.setAcceptInsecureCerts(true);
		WebDriverManager.chromedriver().setup();
		driver1 = new ChromeDriver(options);

		driver1.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
		driver1.manage().window().maximize();
		driver1.get("https://emedWEB:doctorwatson@stg.emed.com/");

		CommonFunctions.logMessage("Navigated to the url https://emedWEB:doctorwatson@stg.emed.com/");

	}
	
	/**
	 * This Method for Navica login
	 * 
	 * @param  navicaUserMail
	 * 
	 * @param  password
	 * 
	 *@throws Exception
	 */

	public void navicalogin(String navicaUserMail, String password) throws Exception {

		CommonFunctions.waitForPageLoad(driver1);
		try {
			if (driver1.getTitle().contains("Homepage")) {
				CommonFunctions.waitForPageLoad(driver1);
			}
		} catch (Exception e) {
			CommonFunctions.logErrorMessagestopExecution("Failed due to page not loaded");
		}
		CommonFunctions.logMessage("<-----eMed Landing Page----->");

		CommonFunctions.waitForPageLoad(driver1);
		new WebDriverWait(driver1, 10).until(ExpectedConditions.elementToBeClickable(loginButton));
		loginButton.click();
		CommonFunctions.logMessage("Login tab is clicked");

		new WebDriverWait(driver1, 10).until(ExpectedConditions.elementToBeClickable(startTesting));
		startTesting.click();
		CommonFunctions.logMessage("Start testing is clicked");

		CommonFunctions.waitForPageLoad(driver1);
		CommonFunctions.logMessage("<-----eMed Login Page----->");

		new WebDriverWait(driver1, 10).until(ExpectedConditions.elementToBeClickable(loginViaNavica));
		loginViaNavica.click();
		CommonFunctions.logMessage("Login via navica is clicked");

		new WebDriverWait(driver1, 20).until(ExpectedConditions.elementToBeClickable(navicaUser));
		navicaUser.click();
		navicaUser.clear();
		navicaUser.sendKeys(navicaUserMail);
		CommonFunctions.logMessage("Navica username is entered");

		new WebDriverWait(driver1, 10).until(ExpectedConditions.elementToBeClickable(navicaPassword));
		navicaPassword.click();
		navicaPassword.clear();
		navicaPassword.sendKeys(password);
		CommonFunctions.logMessage("Navica password is entered");

		new WebDriverWait(driver1, 10).until(ExpectedConditions.elementToBeClickable(navicaSigin));
		navicaSigin.click();
		CommonFunctions.logMessage("Navica signin is clicked");

		try {
			new WebDriverWait(driver1, 10).until(ExpectedConditions.elementToBeClickable(navicaPermissionAllow));
			navicaPermissionAllow.click();
			CommonFunctions.logMessage("Navica permission allow is clicked");
		} catch (Exception e) {
		}

	}
	
	/**
	 * This Method to start the test session
	 * 
	 * 
	 *@throws Exception
	 *
	 *
	 */

	public void startSessionTest() throws Exception {
		new WebDriverWait(driver1, 20).until(ExpectedConditions.elementToBeClickable(startLabTest));
		startLabTest.click();
		CommonFunctions.logMessage("Start lab test is clicked");

		new WebDriverWait(driver1, 20).until(ExpectedConditions.visibilityOf(twentyMinutesTestHeader));
		for (int ques = 0; ques < 9; ques++) {
			CommonFunctions.logMessage("Pre-Test Assess " + (ques + 1) + ": " + preTestQuestions.get(ques).getText());
			List<WebElement> preTestAnswer = driver1
					.findElements(By.xpath("(//form//div[@data-testid])[" + (ques + 1) + "]//label"));
			preTestAnswer.get(0).click();
			CommonFunctions.logMessage("Answer " + preTestAnswer.get(0).getText() + " is clicked");
		}

		WebElement preAssessContinue = driver1.findElement(By.xpath("//button[text()='CONTINUE']"));
		preAssessContinue.click();
		CommonFunctions.logMessage("Pre-Test continue button is clicked");

		CommonFunctions.waitForPageLoad(driver1);
		new WebDriverWait(driver1, 20).until(ExpectedConditions.visibilityOf(checkForWait));
		CommonFunctions.waitForPageLoad(driver1);
		CommonFunctions.logMessage("Virtual lab page is displayed and waiting for certified proctor");
	}
	
	/**
	 * This Method for second Patient browser invocation
	 * 
	 *  @param baseurl
	 *  
	 *  @param username
	 *  
	 *  @param password
	 *  
	 *  
	 *@throws Exception
	 *
	 *
	 */


	public void secondPatientInvoker(String baseurl, String username, String password) throws Exception {
		driverInvoker(baseurl);
		navicalogin(username, password);
		startSessionTest();
	}
}
