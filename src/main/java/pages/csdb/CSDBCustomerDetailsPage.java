package main.java.pages.csdb;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

import main.java.base.TestBase;
import main.java.utils.CommonFunctions;

public class CSDBCustomerDetailsPage extends TestBase {

	private JavascriptExecutor jse = null;

	@FindBy(xpath = "//*[text()='First Name']//following-sibling::p")
	private WebElement customerFirstName;

	@FindBy(xpath = "//*[text()='Last Name']//following-sibling::p")
	private WebElement customerLastName;

	@FindBy(xpath = "//*[text()='Email Address']//following-sibling::a")
	private WebElement customerEmailAddress;

	@FindBy(xpath = "//*[text()='Phone Number']//following-sibling::*[text()]")
	private WebElement customerPhoneNumber;

	@FindBy(xpath = "//*[text()='Date of Birth']//following-sibling::p")
	private WebElement customerDOB;

	@FindBy(xpath = "//*[text()='Address']//parent::div")
	private WebElement customerAddress;

	public CSDBCustomerDetailsPage() {
		jse = (JavascriptExecutor) driver;
		PageFactory.initElements(new AjaxElementLocatorFactory(driver, 10), this);
	}

	JavascriptExecutor e = (JavascriptExecutor) driver;

	/**
	 * @apiNote Method is used to verify the CSDB personal details
	 * @param customerEmail
	 * @throws Exception
	 */
	public void verifyCSDBCustommerPersonalDetails(String customerEmail) throws Exception {

		String firstName = CommonFunctions.getTextOfElement(customerFirstName, "Customer first name");
		String lastName = CommonFunctions.getTextOfElement(customerLastName, "Customer last name");
		String emailAddress = CommonFunctions.getTextOfElement(customerEmailAddress, "Customer email address");
		String dob = CommonFunctions.getTextOfElement(customerDOB, "Customer DOB");

		if (firstName.equalsIgnoreCase(CommonFunctions.getdata("FirstName"))) {
			CommonFunctions.logMessage("Customer registered first name matched the CSDB first name ===> " + firstName);
		} else {
			CommonFunctions.logErrorMessage("Customer registered first name " + CommonFunctions.getdata("FirstName")
					+ " not matched CSDB first name " + firstName);
		}

		if (lastName.equalsIgnoreCase(CommonFunctions.getdata("LastName"))) {
			CommonFunctions.logMessage("Customer registered last name matched the CSDB last name ===> " + lastName);
		} else {
			CommonFunctions.logErrorMessage("Customer registered last name " + CommonFunctions.getdata("LastName")
					+ " not matched CSDB first name " + lastName);
		}

		if (emailAddress.equalsIgnoreCase(customerEmail)) {
			CommonFunctions.logMessage(
					"Customer registered email address matched the CSDB email address ===> " + emailAddress);
		} else {
			CommonFunctions.logErrorMessage("Customer registered email address " + customerEmail
					+ " not matched CSDB email address " + emailAddress);
		}

		String processedDOB = CommonFunctions.monthHashMap(CommonFunctions.getdata("DateofBirth").split("/")[0]) + " "
				+ CommonFunctions.getdata("DateofBirth").split("/")[1].replaceFirst("^(0+)", "") + ", "
				+ CommonFunctions.getdata("DateofBirth").split("/")[2];

		if (dob.equalsIgnoreCase(processedDOB)) {
			CommonFunctions.logMessage("Customer registered date of birth matched the CSDB date of birth ===> " + dob);
		} else {
			CommonFunctions.logErrorMessage("Customer registered date of birth "
					+ CommonFunctions.getdata("DateofBirth") + " not matched CSDB date of birth " + dob);
		}
	}

	/**
	 * @apiNote Method is used to verify the CSDB contact details
	 * @throws Exception
	 */
	public void verifyCustomerContactDetails() throws Exception {

		String phoneNumber = CommonFunctions.getTextOfElement(customerPhoneNumber, "Customer phone number")
				.replaceAll("[()-]+", "");
		String customerFullAddress = CommonFunctions.getTextOfElement(customerAddress, "Customer address")
				.replaceAll("Address|\\n", " ").trim();

		if (phoneNumber.equalsIgnoreCase(CommonFunctions.getdata("PhNumber"))) {
			CommonFunctions
					.logMessage("Customer registered phone number matched the CSDB phone number ===> " + phoneNumber);
		} else {
			CommonFunctions.logErrorMessage("Customer registered phone number " + CommonFunctions.getdata("PhNumber")
					+ " not matched CSDB phone number " + phoneNumber);
		}

		String processedAddress = CommonFunctions.getdata("Address") + " Apt #"
				+ CommonFunctions.getdata("BuildingNumber") + " " + CommonFunctions.getdata("City") + ", "
				+ CommonFunctions.getdata("State") + " " + CommonFunctions.getdata("ZipCode");

		if (customerFullAddress.equalsIgnoreCase(processedAddress)) {
			CommonFunctions
					.logMessage("Customer registered address matched the CSDB address ===> " + customerFullAddress);
		} else {
			CommonFunctions.logErrorMessage("Customer registered address " + processedAddress
					+ " not matched CSDB address " + customerFullAddress);
		}

	}

}
