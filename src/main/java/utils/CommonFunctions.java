package main.java.utils;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.DirectoryNotEmptyException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import java.util.Random;
import java.util.Set;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.imageio.ImageIO;

import org.apache.commons.codec.binary.Base64;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.SkipException;

import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;

import main.java.base.TestBase;
import main.java.utils.reportutil.ExtentManager;

import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;
import ru.yandex.qatools.ashot.shooting.ShootingStrategies;

import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.*;

public class CommonFunctions extends TestBase {
	private static JavascriptExecutor jse = null;

	/**
	 * This method to get the date from todays date
	 * 
	 * @param days We have to pass the current date by passing the number of days
	 * @return
	 */
	public static String getDatefromToday(int days) {

		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, days);
		return dateFormat.format(cal.getTime());
	}

	/**
	 * This method will generate the date based on the GMT Time which help us to
	 * create a flight on the GMT Time
	 * 
	 * @param days
	 * @return
	 */
	public static String getPSTtime(int days) {
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
		sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
		cal.setTime(new Date());
		cal.add(Calendar.HOUR_OF_DAY, days);
		cal.getTime();
		String text = sdf.format(cal.getTime());
		System.out.println(sdf.format(cal.getTime()));
		return text;
	}

	/**
	 * This method will generate the date based on the GMT Time which help us to
	 * create a flight on the GMT Time
	 * 
	 * @param days and date
	 * @return
	 */
	public static String getPSTtime(String date, int days) {
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("" + date + "'T'HH:mm:ss'Z'");
		sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
		cal.setTime(new Date());
		cal.add(Calendar.HOUR_OF_DAY, days);
		cal.getTime();
		String text = sdf.format(cal.getTime());
		System.out.println(sdf.format(cal.getTime()));
		return text;
	}

	/**
	 *
	 * @param messageToLog what are the message we passing that is printing in
	 *                     console
	 */
	public static void logMessage(String messageToLog) {

		try {
			System.out.println(messageToLog);
			test.log(Status.INFO, MarkupHelper.createLabel(messageToLog, ExtentColor.GREEN));
		} catch (Exception e) {
		}
	}

	/**
	 *
	 * @param messageToLog errors that is printing in console
	 */
	public static void logErrorMessage(String messageToLog) {

		try {
			System.err.println(messageToLog);
			if (errorLogCount == 0) {
				scenarioComments.add(totalTest - 1, messageToLog);
				scenarioNo.add(totalTest - 1, CommonFunctions.getdata("Iteration"));
				scenarioName.add(totalTest - 1, CommonFunctions.getdata("ScenarioTitle"));
				scenarioDescription.add(totalTest - 1, CommonFunctions.getdata("Scenario"));
				scenarioStatus.add(totalTest - 1, "Failed");
				failure++;
				errorLogCount++;
			} else {
				scenarioComments.set(totalTest - 1, messageToLog);
			}
			test.log(Status.FAIL, MarkupHelper.createLabel(messageToLog, ExtentColor.RED));
		} catch (Exception e) {
		}
	}

	/**
	 * This method for prints the error messages and stop the execution
	 *
	 * @param messageToLog this is for print the error message and stop the
	 *                     execution
	 * @throws Exception
	 */
	public static void logErrorMessagestopExecution(String messageToLog) throws Exception {
		boolean flag = new CommonFailures(driver).commonErrorsCaptures();
		if (!flag) {
			logErrorMessage(messageToLog);
			throw new Exception();
		}
	}

	/**
	 * This method for prints the skip messages and skip the execution
	 * 
	 * @param messageToLog this is for print the error message and stop the
	 *                     execution
	 */
	public static void logMessageSkipExecution(String messageToLog) {
		scenarioComments.add(messageToLog);
		throw new SkipException(messageToLog);

	}

	/**
	 * @author Mohammed Mustafa This method for scrolling the webpage for the
	 *         webelement specified
	 * 
	 * @param ele - the element to be scrolled to view
	 * @throws Exception
	 */
	public static void scrollIntoView(WebElement ele) {
		try {
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", ele);
		} catch (Exception e) {
			CommonFunctions.logErrorMessage("Exception while scrolling the window");
		}
	}

	/**
	 * This method for take the screenshot for failed test cases *
	 * 
	 * @param msg it is for storing the screenshot with the error message
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public static String getScreenShot() throws Exception {
		Screenshot screenshot = new AShot().shootingStrategy(ShootingStrategies.viewportPasting(1000))
				.takeScreenshot(driver);
		String imgPath = ExtentManager.getFilePath().replace("\\eMed.html", "").replace("/eMed.html", "") + "\\";
		String imageName = "image" + currentMilliseconds() + ".png";
		String screenShotPath = imgPath + imageName;
		try {

			ImageIO.write(screenshot.getImage(), "PNG", new File(screenShotPath));
		} catch (IOException e) {
			System.err.println("Screen Shot failed " + e.getMessage());
		}

		return imageName;
	}

	/**
	 * This method for generating current milliseconds
	 * 
	 * @return
	 */
	public static long currentMilliseconds() throws Exception {

		Date date = new Date();
		SimpleDateFormat DateFor = new SimpleDateFormat();
		long currentMillis = timeMillis(DateFor.format(date));
		return currentMillis;
	}

	/**
	 * This method for generating the milliseconds for the given date and time
	 * 
	 * @throws Exception
	 * @return
	 */
	public static long timeMillis(String dateStr) throws Exception {

		DateFormat dateFormat = new SimpleDateFormat();
		Date date = dateFormat.parse(dateStr);
		long millis = date.getTime();
		return millis;
	}

	/**
	 * @author Mohammed Mustafa This method to switch frame using webelement
	 *
	 * @throws Exception
	 */
	public void switchToFrameElement(WebElement FrameElement) {
		driver.switchTo().frame(FrameElement);
	}

	public static void actionClick(WebElement webele, String Ele_name) throws Exception {
		try {
			new WebDriverWait(driver, 10).until(ExpectedConditions.elementToBeClickable(webele));
			Actions ob = new Actions(driver);
			Action action = ob.moveToElement(webele).click().build();
			action.perform();
			logMessage("The " + Ele_name + " is clicked");
		} catch (Exception e) {
			logErrorMessagestopExecution("The " + Ele_name + " is not avilable pls refer the report\n");
		}
	}

	/**
	 * This method for Clicking the Web element
	 * 
	 * @throws Exception
	 **/
	public static void clickWebelement(WebElement webele, String Ele_name) throws Exception {
		try {
			new WebDriverWait(driver, 30).until(ExpectedConditions.elementToBeClickable(webele));
			JavascriptExecutor jse = (JavascriptExecutor) driver;
			jse.executeScript("arguments[0].click();", webele);
			logMessage("The " + Ele_name + " is clicked");
		} catch (Exception e) {
			logErrorMessagestopExecution("The " + Ele_name + " is not avilable pls refer the screenshot\n");
		}
	}

	/**
	 * This method for Sending the values to the Web element
	 * 
	 * @throws Exception
	 **/
	public static void Sendkeys(WebElement webele, String value, String Ele_name) throws Exception {
		try {
			new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(webele));
			webele.clear();
			webele.sendKeys(value);
			logMessage(value + " is entered into the " + Ele_name + " textbox");
		} catch (Exception e) {
			logErrorMessagestopExecution("\nThe " + Ele_name + " is not avilable pls refer the screenshot\n");
		}

	}

	/**
	 * @author Mohammed Mustafa This method for Sending the values to the Web
	 *         element and without any log message
	 * 
	 * @throws Exception
	 **/
	public static void SendkeysWithoutInputLog(WebElement webele, String value, String Ele_name) throws Exception {
		try {
			new WebDriverWait(driver, 20).until(ExpectedConditions.visibilityOf(webele));
			webele.click();
			webele.clear();
			webele.sendKeys(value);
			logMessage("Authentic " + Ele_name + " is entered into the " + Ele_name + " textbox");
		} catch (Exception e) {
			logErrorMessagestopExecution("\nThe " + Ele_name + " is not avilable pls refer the report\n");
		}

	}

	/**
	 * @author Mohammed Mustafa
	 * 
	 * @apiNote This method for Sending the values to the Web element by clearing
	 *          field using attribute
	 * 
	 * @param webele
	 * @param attrib
	 * @param value
	 * @param Ele_name
	 * 
	 * @throws Exception
	 **/
	public static void SendkeysAttrib(WebElement webele, String attrib, String value, String Ele_name)
			throws Exception {
		try {
			new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(webele));
			webele.click();
			webele.clear();
			while (!webele.getAttribute(attrib).equals("")) {
				webele.sendKeys(Keys.BACK_SPACE);
			}
			webele.sendKeys(value);
			logMessage(value + " is entered into the " + Ele_name + " textbox");
		} catch (Exception e) {
			logErrorMessagestopExecution("\nThe " + Ele_name + " is not avilable pls refer the screenshot\n");
		}

	}

	/**
	 * @author Mohammed Mustafa This method for selecting the dropdown value using
	 *         select class
	 * 
	 * @throws Exception
	 **/
	public static void selectDropDownValue(WebElement webele, String value, String Ele_name) throws Exception {
		try {
			new WebDriverWait(driver, 20).until(ExpectedConditions.visibilityOf(webele));
			Select select = new Select(webele);
			select.selectByValue(value);
			logMessage(value + " is selected into the " + Ele_name + " dropdown");
		} catch (Exception e) {
			logErrorMessagestopExecution("\nThe " + Ele_name + " is not avilable pls refer the report\n");
		}

	}

	/**
	 * @author Mohammed Mustafa This method for selecting the dropdown text using
	 *         select class
	 * 
	 * @throws Exception
	 **/
	public static void selectDropDownText(WebElement webele, String value, String Ele_name) throws Exception {
		try {
			new WebDriverWait(driver, 20).until(ExpectedConditions.visibilityOf(webele));
			Select select = new Select(webele);
			select.selectByVisibleText(value);
			logMessage(value + " is selected into the " + Ele_name + " dropdown");
		} catch (Exception e) {
			logErrorMessagestopExecution("\nThe " + Ele_name + " is not avilable pls refer the report\n");
		}

	}

	/**
	 * This method for checking the page is displayed or Not
	 * 
	 * @param webele We have to pass the web element to check that is Clickable or
	 *               not
	 * @return
	 */
	public static boolean checkCurrentPage(WebElement webele) {
		try {
			new WebDriverWait(driver, 7).until(ExpectedConditions.elementToBeClickable(webele));
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * This Method for checking the page is loaded or not and return the boolean
	 * value true or false
	 * 
	 * @param driver
	 * @throws InterruptedException
	 */
	public static void waitForPageLoad(WebDriver driver) throws InterruptedException {
		Thread.sleep(4000);
		ExpectedCondition<Boolean> pageLoadCondition = new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver driver) {
				return ((JavascriptExecutor) driver).executeScript("return document.readyState").equals("complete");
			}

		};
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(pageLoadCondition);
	}

	/**
	 * This method for getting the data from the hash map and returns the value
	 * 
	 * @param Name It is the name of the column
	 * @return
	 * @throws Exception
	 */
	public static String getdata(String Name) throws Exception {

		String data = "";
		if (inputdata.hashmap().containsKey(Name)) {
			data = inputdata.hashmap().get(Name);
		} else {
			CommonFunctions.logErrorMessagestopExecution("Given Column name is not availale in the Excel " + Name);
		}
		return data;

	}

	/**
	 * @author Mohammed Mustafa
	 * 
	 *         This method for generating the Email randomly based on
	 *         timestampMilliseconds
	 */
	public static String EmailGenTimeStamp(String labelName, String domainName) {

		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		String eMail = labelName + timestamp.getTime() + "@" + domainName;
		return eMail;
	}

	/**
	 * This method for switching the one window to next window
	 * 
	 * @param driver
	 * @throws InterruptedException
	 */
	public static void switchNextWindow(WebDriver driver) throws InterruptedException {
		Thread.sleep(4000);
		int i = 0;
		for (String Child_Window : driver.getWindowHandles()) {
			if (i > 0) {
				driver.switchTo().window(Child_Window);
			}
			i++;
		}
	}

	/**
	 * This Method for checking the web element is visible or not
	 * 
	 * @param webele pass the web element.
	 * @param name   we have to pass the name of the element if it is not displayed
	 *               it prints the error message
	 * @throws Exception
	 */
	public static void elementIsVisible(WebElement webele, String name) throws Exception {
		try {
			new WebDriverWait(driver, 20).until(ExpectedConditions.visibilityOf(webele));
		} catch (Exception e) {
			logErrorMessagestopExecution("The Object " + name + " is not available.");
		}

	}

	/**
	 * @author Mohammed Mustafa This Method for checking the web element is visible
	 *         or not for given timer
	 * 
	 * @param webele  pass the web element.
	 * @param timeOut Timeout for the element to visible
	 * 
	 * @return boolean value
	 * @throws Exception
	 */
	public static boolean elementVisibleToCheck(WebElement webele, int timeOut) throws Exception {
		try {
			new WebDriverWait(driver, timeOut).until(ExpectedConditions.visibilityOf(webele));
			return true;
		} catch (Exception e) {
			return false;
		}

	}

	/**
	 * This Method for checking the web element is clickable or not
	 * 
	 * @param ele     pass the web element.
	 * @param objname we have to pass the name of the element if it is not displayed
	 *                it prints the error message
	 * @throws Exception
	 */
	public static void elementToBeClickable(WebElement ele, String objname) throws Exception {

		try {
			new WebDriverWait(driver, 10).until(ExpectedConditions.elementToBeClickable(ele));
		} catch (Exception e) {
			logErrorMessagestopExecution("The object " + objname + " is not clickable");
		}
	}

	/**
	 * @author Mohammed Mustafa This method for checks for element exist or not
	 * 
	 * @return
	 */
	public static boolean isExist(WebDriver driverPass, String string) throws Exception {
		boolean check = false;
		try {
			WebDriverWait wait = new WebDriverWait(driverPass, 5);
			wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(string)));
			wait.ignoring(NoSuchElementException.class);
			check = true;
		} catch (TimeoutException e) {
			check = false;
		}
		return check;
	}

	/**
	 * This method for converting the date from one format to another format
	 * 
	 * @param OldFormat   We have to pass the current date format dd/MM/YYYY
	 * @param NewFormat   We have to pass the Expected date format MM/dd/YYYY
	 * @param PaasingDate We have to pass the date
	 * @return
	 * @throws Exception
	 */
	public static String convertDateOneFormatToAnother(String OldFormat, String NewFormat, String PaasingDate)
			throws Exception {
		try {
			String result = "";
			SimpleDateFormat OldDateFormate = new SimpleDateFormat(OldFormat);
			SimpleDateFormat NewDateFormate = new SimpleDateFormat(NewFormat);
			result = NewDateFormate.format(OldDateFormate.parse(PaasingDate));
			return result;
		} catch (Exception e) {
			logErrorMessagestopExecution("Error While converting the date pls check the format of the date");
			return PaasingDate;
		}

	}

	/**
	 * This method for checking the page is displayed or Not using page title
	 *
	 *
	 * @throws Exception
	 */
	public static void checkCurrentPageTitle(String pageName) throws Exception {
		try {
			if (driver.getTitle().contains(pageName)) {
				waitForPageLoad(driver);
			}
		} catch (Exception e) {
			logErrorMessagestopExecution("Failed due to page not loaded");
		}
	}

	/**
	 * This method for checking the page is displayed or Not
	 * 
	 * @param webele We have to pass the web element to check that is Clickable or
	 *               not
	 * @throws Exception
	 */
	public static void checkCurrentPage(WebElement webele, String pageName) throws Exception {
		try {
			new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOf(webele));
			if (!webele.getText().contains(pageName)) {
				waitForPageLoad(driver);
			}
		} catch (Exception e) {
			logErrorMessagestopExecution("Failed due to page not loaded");
		}
	}

	/**
	 * This method for checking the passing dynamic xpath is available or not and
	 * returns the web element element
	 * 
	 * @param driver
	 * @param attributevalue We have to pass the dynamic xpath to check if it is
	 *                       available or not
	 * @param objName        we have to pass the name of the element if it is not
	 *                       displayed it prints the error message
	 * @return
	 * @throws Exception
	 */
	public static WebElement findElementByXpath(WebDriver driver, String attributevalue, String objName)
			throws Exception {
		WebElement element = null;
		try {
			element = driver.findElement(By.xpath(attributevalue));
			return element;
		} catch (Exception ex) {
			CommonFunctions.logErrorMessagestopExecution("The Object " + objName + " is not available.");
			return null;
		}
	}

	/**
	 * @param webele
	 * @param objName
	 * @throws Exception
	 */
	public static String getTextOfElement(WebElement webele, String objName) throws Exception {
		String elementText = null;
		try {
			new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(webele));
			elementText = webele.getText();
		} catch (Exception e) {
			logErrorMessagestopExecution("The Object " + objName + " is not available.");
		}
		return elementText;
	}

	/**
	 * @param webele
	 * @param objName
	 * @param attributeName This method will Get the Attribute of the Element
	 * @return
	 */
	public static String getAttributeOfElement(WebElement webele, String objName, String attributeName) {
		String element = null;
		try {
			new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(webele));
			element = webele.getAttribute(attributeName);
		} catch (Exception e) {
			logErrorMessage("The Object " + objName + " is not available.");
		}
		return element;
	}

	/**
	 * This method for Switching from one frame another frame using frame name
	 * 
	 *
	 * @param framename We have to pass the frameId to check if it is available or
	 *                  not
	 * @return
	 * @throws Exception
	 */
	public static void switchFrame(String framename) {

		try {
			driver.switchTo().defaultContent();
			driver.switchTo().frame(framename);
		} catch (Throwable ex) {
			System.out.println("Error occured while switching the frame");
			System.out.println(ex.getMessage());
		}
	}

	/**
	 * This Method for checking the web element is visible or not
	 * 
	 * @param attributeValue pass the web element.
	 * @param name           we have to pass the name of the element if it is not
	 *                       displayed it prints the error message
	 * @throws Exception
	 */
	@SuppressWarnings("unused")
	public static boolean dynamicElementIsVisible(String attributeValue, String name) throws Exception {
		WebElement element = null;
		try {
			element = driver.findElement(By.xpath(attributeValue));
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * @author Mohammed Mustafa
	 * 
	 * @param charsLens --> Passing the String len to be generated
	 * 
	 *                  This method for generating random string
	 */
	public static String getRandomString(int charsLen) {
		String CharSet = "abcdefghijkmnopqrstuvwxyz";
		String name = "";
		for (int a = 1; a <= charsLen; a++) {
			name += CharSet.charAt(new Random().nextInt(CharSet.length()));
		}
		return name;
	}

	/**
	 * This method for switching to active element
	 */
	public static void switchToActiveElement() {
		driver.switchTo().activeElement();
	}

	/**
	 * @param password --> Send the Encoded password
	 * @return It's return the decoded Password This method is used for decode the
	 *         password and return to the application
	 */
	public static String decodepass(String password) {
		String token = null;
		if (!password.equals(null))
			token = new String(Base64.decodeBase64(password.getBytes()));

		if (!token.equals(null))
			password = token;
		return password;

	}

	/**
	 * @author Mohammed Mustafa
	 * 
	 *         This method for typing the text in text style with dynamic timeout
	 *         value
	 * 
	 * @param driver --> driver invoked value
	 * 
	 * @throws ElementNotVisibleException
	 */
	public static void typeTextWithTimeOut(WebDriver driver, WebElement locator, String text, Integer timeOut) {
		try {
			WebDriverWait wait = new WebDriverWait(driver, timeOut);
			wait.until(ExpectedConditions.elementToBeClickable(locator));

			Actions builder = new Actions(driver);
			Action seriesOfActions = builder.moveToElement(locator).click().sendKeys(locator, text).click()
					.sendKeys(locator, Keys.TAB).build();
			seriesOfActions.perform();
			logMessage(text + " is entered into the " + locator + " textbox");
		} catch (ElementNotVisibleException e) {
			logErrorMessage(String.format("Expected element %s was not found", locator.toString()));
		}
	}

	/**
	 * @author Mohammed Mustafa
	 * 
	 *         This method for typing text using action class
	 * 
	 * @throws ElementClickInterceptedException
	 */
	public static void typeText(WebDriver driver, WebElement locator, String text, String Ele_name) {
		try {
			Actions builder = new Actions(driver);
			Action seriesOfActions = builder.moveToElement(locator).click().sendKeys(locator, text).click()
					.sendKeys(locator, Keys.TAB).build();
			seriesOfActions.perform();
			logMessage(text + " is entered into the " + Ele_name + " textbox");
		} catch (ElementClickInterceptedException e) {
			logErrorMessage(String.format("Expected element %s was not found", locator.toString()));
		}
	}

	/**
	 * This method for clicking the element using javascript click
	 * 
	 * @throws WebDriverException
	 */
	public static void clickJSE(WebElement ele, String elementName) {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.visibilityOf(ele));
			wait.until(ExpectedConditions.elementToBeClickable(ele));
			jse = (JavascriptExecutor) driver;
			jse.executeScript("arguments[0].click();", ele);
			logMessage("iClick on the element: " + elementName);
		} catch (WebDriverException e) {
			logErrorMessage("i failed to Click on the element: " + elementName);
		}
	}

	/**
	 * @author Mohammed Mustafa This method used to iterate string on given
	 *         condition and click the element
	 * @exception Exception
	 **/
	public static void iterateElementClick(List<WebElement> webele, String cond) throws Exception {
		boolean flag = false;
		for (int dte = 0; dte < webele.size(); dte++) {
			if (webele.get(dte).getText().contains(cond)) {
				webele.get(dte).click();
				Thread.sleep(100);
				logMessage(cond + " is selected");
				flag = true;
				break;
			}
		}
		if (!flag) {
			logErrorMessagestopExecution("Element " + cond + " searched is not available");
		}
	}

	/**
	 * @author Mohammed Mustafa
	 * 
	 * @apiNote Method is used to enter the string individually by character
	 * 
	 * @param ele         --> WebElement
	 * @param elementName --> String that to be individually entered
	 * @param strName     --> String to be printed as output
	 */
	public static void sendKeysIndividual(WebElement ele, String elementName, String strName) {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.visibilityOf(ele));
			wait.until(ExpectedConditions.elementToBeClickable(ele));
			ele.click();
			for (int i = 0; i < elementName.length(); i++) {
				String strChar = new StringBuilder().append(elementName.charAt(i)).toString();
				ele.sendKeys(strChar);
			}
			logMessage(elementName + " is entered into the " + strName + " textbox");
		} catch (Exception e) {
			logErrorMessage("\nThe " + strName + " is not avilable pls refer the screenshot\n");
		}
	}

	/**
	 * @author Mohammed Mustafa This method is to invoke new webpage in new tab
	 * @param url    --> url to be invoked
	 * @param driver --> driver value for the current invoked driver
	 */
	public static void openNewBrowserTab(String url, WebDriver driver) throws InterruptedException {
		((JavascriptExecutor) driver).executeScript("window.open('" + url + "')");
		logMessage("Navigated to " + url);
		switchNextWindow(driver);
		driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
	}

	/**
	 * @author Mohammed Mustafa This method is to switch browser tab for a specified
	 *         index
	 * @param window --> window index value to be switched
	 * @param driver --> driver value for the current invoked driver
	 */
	public static void moveToSpecifiedWindow(WebDriver driver, int window) {
		Set<String> winSet = driver.getWindowHandles();
		List<String> winList = new ArrayList<String>(winSet);
		String newTab = winList.get(window);
		driver.switchTo().window(newTab);
	}

	/**
	 * @author Mohammed Mustafa This method is to get the total count of windows
	 *         opened
	 * 
	 * @param driver --> driver value for the current invoked driver
	 * @return
	 */
	public static int getCurrentWindowsCount(WebDriver driver) {
		Set<String> winSet = driver.getWindowHandles();
		List<String> winList = new ArrayList<String>(winSet);
		return winList.size();
	}

	/**
	 *
	 * This Method is used for Copy clipboard using robot key
	 * 
	 * @param text --> text to be copied
	 * 
	 */

	public static void robotKeyClipboard(String text) throws AWTException {
		StringSelection stringSelection = new StringSelection(text);
		Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
		clipboard.setContents(stringSelection, stringSelection);

		Robot robot = new Robot();
		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_V);
		robot.keyRelease(KeyEvent.VK_CONTROL);
	}

	/**
	 *
	 * This Method is used for Browser authenticate using robot key
	 * 
	 * @param strName --> Name of the userName
	 * @param strName --> Password
	 */

	public static void robotBrowserAuthenticate(String userName, String password)
			throws InterruptedException, AWTException {
		Thread.sleep(5000);
		Robot robot = new Robot();
		CommonFunctions.robotKeyClipboard(userName);
		CommonFunctions.logMessage("Basic authorization username entered");
		Thread.sleep(1000);
		robot.keyPress(KeyEvent.VK_TAB);
		Thread.sleep(1000);
		CommonFunctions.robotKeyClipboard(password);
		CommonFunctions.logMessage("Basic authorization password entered");
		Thread.sleep(1000);
		robot.keyPress(KeyEvent.VK_ENTER);
		Thread.sleep(500);
		robot.keyRelease(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_TAB);
	}

	/**
	 * @author Mohammed Mustafa This method for kill the all opened browsers
	 * 
	 * @throws IOException
	 */
	public static void killBrowser() throws IOException {
		try {
			String os = System.getProperty("os.name");
			CommonFunctions.logMessage("OS Name - " + os);
			if (os.contains("Windows")) {
				Runtime.getRuntime().exec("taskkill /F /IM firefox.exe");
				Runtime.getRuntime().exec("taskkill /F /IM IEDriverServer.exe");
				Runtime.getRuntime().exec("taskkill /F /IM chrome.exe");
			}
			if (os.contains("Mac")) {
				Runtime.getRuntime().exec("pkill firefox");
				Runtime.getRuntime().exec("pkill safari");
				Runtime.getRuntime().exec("pkill chrome");
			}
			CommonFunctions.logMessage("Killing the browsers which already opened...");
			Thread.sleep(2000);
		} catch (Exception e) {
			CommonFunctions.logErrorMessage("Exception while closing the browsers");
		}
	}

	/**
	 * @author Mohammed Mustafa This method for fetching the values from the
	 *         Credentials properties file
	 * 
	 * @throws IOException
	 */
	public static Properties getPropertyValues() {

		File file = new File(System.getProperty("user.dir") + "//src//main//resources//Credentials.properties");

		FileInputStream fileInput = null;
		try {
			fileInput = new FileInputStream(file);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		Properties prop = new Properties();

		// loads properties file
		try {
			prop.load(fileInput);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return prop;
	}

	/**
	 * @author Mohammed Mustafa This method for store the text month in hash map and
	 *         get by passing key value of month in number
	 * 
	 * @param keyMonthNo
	 * 
	 **/
	public static String monthHashMap(String keyMonthNo) {
		HashMap<String, String> month = new HashMap<String, String>();
		month.put("01", "January");
		month.put("02", "February");
		month.put("03", "March");
		month.put("04", "April");
		month.put("05", "May");
		month.put("06", "June");
		month.put("07", "July");
		month.put("08", "August");
		month.put("09", "September");
		month.put("10", "October");
		month.put("11", "November");
		month.put("12", "December");
		return month.get(keyMonthNo);

	}

	/**
	 * @author Mohammed Mustafa
	 * 
	 * @apiNote Method used to extract the value from the string using the regex
	 *          pattern
	 * 
	 * @throws Exception
	 * @param regexPattern
	 * @param matchStr
	 * 
	 * @return
	 */
	public static String regexText(String regexPattern, String matchStr) throws Exception {
		String result = "";
		try {
			Pattern regex = Pattern.compile(regexPattern);
			Matcher match = regex.matcher(matchStr);
			if (match.find()) {
				result = match.group(0);
			}
			return result;
		} catch (Exception e) {
			CommonFunctions
					.logErrorMessagestopExecution("Error while regexing/mismatch with text and regex pattern given");
		}
		return result;
	}

	/**
	 * @author Mohammed Mustafa
	 * 
	 *         This method is used to move the file to specific path
	 * 
	 * @param srcPath
	 * @param targetPath
	 * 
	 * @throws {@link NoSuchFileException}, {@link DirectoryNotEmptyException},
	 *                {@link IOException}
	 */
	public static void moveFileToSpecificPath(String srcPath, String targetPath) {
		try {
			Path source = Paths.get(srcPath);
			Path target = Paths.get(targetPath);
			Files.move(source, target);

		} catch (NoSuchFileException x) {
			System.err.format("No such file exist");

		} catch (DirectoryNotEmptyException x) {
			System.err.format("Directory is empty");

		} catch (IOException x) {
			System.err.println(x);
		}
	}

}
