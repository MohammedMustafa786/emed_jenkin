package main.java.base;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.TimeZone;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.DataProvider;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;

import main.java.utils.xlservice.XlsReader;

public class TestBase {

	public static int count = -1;
	public static int searchCount = 0;
	public static boolean skip = false;
	public static boolean fail = false;

	public static XlsReader AAxls = null;
	public static String sheetName = "";
	public static boolean isInitialized = false;
	public static WebDriver driver;
	public static WebDriver driver1;
	public static String Excelpath = "";
	public static XlsReader inputdata;
	public static boolean isTestPass = true;
	public static String downloadFilePath = "";
	public static String uploadFilePath = "";
	public static String excelPath = "";
	public static String extentReportPath = "";
	private String browserNeeded = null;
	/*
	 * Browser Configuration
	 */
	public static String[] browser = System.getenv("Browser").split(":");
	public static String BrowserNeed = browser[0];
	public static String mode = browser[1];
	public static String SheetName = System.getenv("TEST_XLS").replace(".xlsx", "");

	/*
	 * Proctor Authentication
	 */
	public static String[] proctorCredentials = System.getenv("PROC").split(":");
	public static String proctorUserCred = proctorCredentials[0];
	public static String proctorPasswordCred = proctorCredentials[1];

	/*
	 * Email report & Selecting the env type
	 */
	public static String env = System.getenv("ENV");

	public static boolean isExcelSkip = false;
	public static int errorLogCount = 0;
	public static int isFirst = 1;
	public static int totalTest = 0;
	public static int passed = 0;
	public static int failure = 0;
	public static LocalTime startTime;

	/*
	 * Email report variables
	 */
	
	public static List<String> scenarioComments = new ArrayList<String>();
	public static List<String> scenarioNo = new ArrayList<String>();
	public static List<String> scenarioName = new ArrayList<String>();
	public static List<String> scenarioDescription = new ArrayList<String>();
	public static List<String> scenarioStatus = new ArrayList<String>();

	protected static ExtentTest test;
	protected static ExtentTest logger;
	protected static ExtentReports extent;

	/*
	 * All global variables Email verification
	 */
	public static String verifyCode = "";
	public static String emailId = "";
	public static String navicaEmail = "";
	public static String navicaVerifyCode = "";
	
	/*
	 * Payment Page
	 */
	public static String orderSubRate = "";
	public static String orderShippingRate = "";
	public static String orderTaxRate = "";
	public static String orderTotalAmount = "";

	/*
	 * ReviewAndConfirmation Page
	 */
	public static String confirmationNum = "";
	public static String OrderId = "";
	
	/*
	 * MyOrder Page
	 */
	public static String orderedDate = "";
	
	/*
	 * Virtual Lab session
	 */

	public static String dependentPatientName = "";
	/*
	 * Initializing the path for the resource files
	 */
	public void pathInitialize() {

		String osName = System.getProperty("os.name");
		if (osName.contains("Win")) {
			downloadFilePath = System.getProperty("user.dir") + "\\downloadDirectory";
			uploadFilePath = System.getProperty("user.dir") + "\\src\\main\\resources\\uploadFiles\\";
			excelPath = System.getProperty("user.dir") + "\\src\\main\\resources\\xlSheets\\";

		} else if (osName.contains("Mac")) {
			downloadFilePath = System.getProperty("user.dir") + "/downloadDirectory";
			uploadFilePath = System.getProperty("user.dir") + "/src/main/resources/uploadFiles/";
			excelPath = System.getProperty("user.dir") + "/src/main/resources/xlSheets/";
		}
	}

	/*
	 * Initializing the Test /**
	 * 
	 * @throws Exception This method for to initialize the excel sheet
	 */
	public void initialize() throws Exception {
		
		pathInitialize();
		if (!isInitialized) {

			String testXLS = System.getenv("TEST_XLS");
			System.out.println("Running Excel sheet " + testXLS);

			if (testXLS != null | testXLS.length() != 0) {
				AAxls = new XlsReader(excelPath + testXLS);
				Excelpath = excelPath + testXLS;
			} else {
				System.out.println("TEST_XLS Environment Variable is not Configured");
			}
			isInitialized = true;

		}
	}

	/**
	 * @return This method for getting the current date in GMT and return the date
	 *         and time
	 */
	public static String currentDateTimeInGMT() {

		Date now = new Date();
		DateFormat converter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssX");

		// getting GMT timezone, you can get any timezone e.g. UTC
		converter.setTimeZone(TimeZone.getTimeZone("GMT"));
		String dateTime1 = converter.format(now);

		return dateTime1;
	}

	/**
	 * @return This method for getting the current date,Time and return the date and
	 *         time
	 */
	public static String currentDateTime() {

		Date now = new Date();
		DateFormat converter = new SimpleDateFormat("DD/MM/YYYY:HH:mm:ss");

		// getting GMT timezone, you can get any timezone e.g. UTC
		converter.setTimeZone(TimeZone.getTimeZone("GMT"));
		String dateTime1 = converter.format(now);

		return dateTime1;
	}

	/* @author Mohammed Mustafa 
	 * This method will delete the eMed automation report and other unwanted files present in test-output folder
	 * 
	 */
	public void deleteReportFiles() throws IOException {

		File directory = new File(System.getProperty("user.dir") + "//test-output//");

		FileUtils.deleteDirectory(directory);

		if (!directory.exists()) {
			System.out.println("Test-output files are deleted successfully");
			directory.mkdir();
		}else {
			System.err.println("Test-output folder and files are not deleted");
		}
	}

	@DataProvider
	public static Iterator<Object[]> getTestData() throws IOException {

		ArrayList<Object[]> data = new ArrayList<Object[]>();

		for (int i = 1; i < AAxls.getRowCount(sheetName); i++) {
			data.add(new Object[] { new XlsReader(Excelpath, sheetName, i) });

		}

		return data.iterator();
	}

	@AfterTest
	public void reportFlush() {
		driver.quit();
		extent.flush();
	}

}
