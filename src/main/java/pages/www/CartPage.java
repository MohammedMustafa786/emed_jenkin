package main.java.pages.www;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

import main.java.base.TestBase;
import main.java.utils.CommonFunctions;

public class CartPage extends TestBase {

	private JavascriptExecutor jse = null;
	
	@FindBy(xpath = "//button[text()='Proceed to Checkout']")
	private WebElement proceedCheckout;

	public CartPage() {
		jse = (JavascriptExecutor) driver;
		PageFactory.initElements(new AjaxElementLocatorFactory(driver, 10), this);
	}

	JavascriptExecutor e = (JavascriptExecutor) driver;
	
	/**
	 * Method to click the proceed checkout
	 * 
	 * 
	 * @throws Exception
	 *
	 */

	public void proceedToCheckout() throws Exception {

		CommonFunctions.elementToBeClickable(proceedCheckout, "proceed to checkout button");
		WebElement proceedToCheckout = driver.findElement(By.xpath("//button[text()='Proceed to Checkout']"));
		proceedToCheckout.click();
		CommonFunctions.logMessage("proceed to checkout button is clicked");
	}
}
